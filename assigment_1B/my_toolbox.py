import datetime as dt
import csv
import matplotlib.pyplot as plt


def clear_screen(x):
    print("\n" * x)
    print('screen cleared')


def mean(data_list):
    sum = 0.0
    for num in data_list:
        sum = sum + num
    return sum / len(data_list)


def is_up(current_price, previous_price):
    return current_price > previous_price


def in_range_date(date, start_date, end_date):
    return date >= start_date and date <= end_date


def filter_by_date(data, start_date, end_date, property="date"):
    res = []
    for item in data:
        if in_range_date(item[property], start_date, end_date):
            res.append(item)
    return res


def parse_stock_data_problem1(data):
    stock_data = []
    for line in data:
        line = line.strip()
        raw_data = line.split(',')
        date = parse_date_str(raw_data[0], 1)
        cprice = float(raw_data[1])
        parsed = {
            "date": date,
            "cprice": cprice
        }
        stock_data.append(parsed)
    return stock_data


def plot_stats(stats):
    mean_list = pick(stats, "mean", formatter=float)
    median_list = pick(stats, "median", formatter=float)
    date_list = pick(stats, "date")
    cprice_list = pick(stats, "cprice", formatter=float)
    gain_list = pick(stats, "gain", formatter=float)

    plt.subplot(2, 2, 1)
    plt.plot(date_list, mean_list)

    plt.subplot(2, 2, 2)
    plt.plot(date_list,  median_list)
    plt.subplot(2, 2, 3)
    plt.plot(date_list,  cprice_list)
    plt.subplot(2, 2, 4)
    plt.plot(date_list,  gain_list)

    plt.show()


def input_dates():
    print("Please input start and end date. Format: yy/mm/dd")
    # start_date = input("Enter start date: ")
    # end_date = input("Enter end date: ")
    start_date = "2004/01/01"
    end_date = "2013/12/31"

    try:
        start_date = dt.datetime.strptime(
            start_date, "%Y/%m/%d").strftime("%Y/%m/%d")
        end_date = dt.datetime.strptime(
            end_date, "%Y/%m/%d").strftime("%Y/%m/%d")
        return start_date, end_date
    except ValueError:
        print("Invalid date format. Please reenter.")
        input_dates()


def pick(data_list, property, formatter=None):
    res = []
    for item in data_list:
        tmp = item[property]
        if formatter is not None:
            tmp = formatter(tmp)
        res.append(tmp)
    return res


def write_to_csv(file_name, data):
    with open(file_name, mode='w') as csv_file:
        fieldnames = ['date', 'cprice', 'mean', 'median', 'is_up', 'gain']
        writer = csv.DictWriter(csv_file, fieldnames=fieldnames)

        writer.writeheader()
        for item in data:
            writer.writerow(item)


def parse_stock_data_problem2(file_name):
    res = []
    with open(file_name) as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=',')
        line_count = 0
        for row in csv_reader:
            if line_count == 0:
                line_count += 1
            else:
                # raw_date, open, high, low, cprice, volume = row
                raw_date, oprice, high, low, cprice, volume = row
                tmp = {
                    "date": parse_date_str(raw_date, problem=2),
                    "open": oprice,
                    "high": high,
                    "low": low,
                    "cprice": cprice,
                    "volume": volume
                }
                res.append(tmp)
                line_count += 1

    return res


def parse_stock_file(file_name, problem=1):
    if problem == 1:
        return parse_stock_data_problem1(open_txt_file(file_name))
    if problem == 2:
        return parse_stock_data_problem2(file_name)
    else:
        return Exception("Invalid problem number")


def parse_stock_data(content, problem=1):
    if problem == 1:
        return parse_stock_data_problem1(content)
    if problem == 2:
        return parse_stock_data_problem2(content)


def list_to_ups_or_downs(data_list):
    res = []
    for i in range(len(data_list)):
        if(i == 0):
            res.append("up")
            continue
        if is_up(data_list[i]["cprice"], data_list[i-1]["cprice"]):
            res.append("up")
        else:
            res.append("down")
    return res


def stock_list_to_changes(data_list):
    res = []
    for i in range(len(data_list)):
        if(i == 0):
            res.append(0)
            continue
        current_price = data_list[i]["cprice"]
        previous_price = data_list[i-1]["cprice"]
        change = (current_price - previous_price) / previous_price * 100
        res.append(change)
    return res


def calculate_gain(current_price, last_price):
    return (float(current_price) - float(last_price)) / float(last_price) * 100


def stock_data_to_date_stats(data):
    res = []
    for i in range(len(data)):
        tmp = {}

        cur = data[i]
        prev = data[i-1]
        current_price = cur["cprice"]
        previous_price = prev["cprice"]
        if(i == 0):
            tmp = {
                "date": cur["date"],
                "cprice": cur["cprice"],
                "mean": cur["cprice"],
                "median": cur["cprice"],
                "is_up": 0,
                "gain": 0
            }
            res.append(tmp)
            continue
        tmp = {
            "date": cur["date"],
            "cprice": cur["cprice"],
            "mean": mean(pick(res, "cprice", formatter=float)),
            "median": mean(pick(res, "cprice", formatter=float)),
            "is_up": is_up(current_price, previous_price),
            "gain": calculate_gain(current_price, previous_price)
        }
        res.append(tmp)
    return res


def get_data_list():
    nums = []
    xStr = input("Enter a number: ")
    while xStr != "":
        x = float(xStr)
        nums.append(x)
        xStr = input("Enter a number: ")
    return nums


def median(data_list):
    data_list.sort()
    size = len(data_list)
    midPos = size // 2

    if size % 2 == 0:
        median = (data_list[midPos] + data_list[midPos-1])/2
    else:
        median = data_list[midPos]
    return median


def open_txt_file(file_name):
    file = open(file_name, 'r')
    data = file.readlines()
    file.close()
    return data


def parse_stock_data(data):
    stock_data = []
    for line in data:
        line = line.strip()
        raw_data = line.split(',')
        date = raw_data[0]
        cprice = float(raw_data[1])
        parsed = {
            "date": date,
            "cprice": cprice
        }
        stock_data.append(parsed)
    return stock_data


def parse_date_str(date_str, problem=1, result_format="%Y/%m/%d"):
    if type(date_str) != "<class 'str'>":
        return date_str
    if problem == 1:
        return dt.datetime.strptime(date_str, '%y%m%d').strftime(result_format)
    if problem == 2:
        return dt.datetime.strptime(date_str, '%m/%d/%Y').strftime(result_format)
    else:
        return Exception("Invalid problem number")
