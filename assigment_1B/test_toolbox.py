import datetime
import my_toolbox as mtb
# import matplotlib.pyplot as plt
# import matplotlib.dates as mdates
# import numpy as np
# import datetime as dt


def problem_2():
    stock_data = mtb.parse_stock_file('Stock_Data_2_v2.csv', problem=2)
    stock_data.pop(0)
    start_date, end_date = mtb.input_dates()
    # stock_data = mtb.filter_by_date(stock_data, start_date, end_date)
    stock_data.sort(key=lambda x: x["date"])
    stats = mtb.stock_data_to_date_stats(stock_data)
    print(stats)
    mtb.write_to_csv("output_problem_2.csv", stats)
    mtb.plot_stats(stats)


def problem_1():
    stock_data = mtb.parse_stock_file('stock_data_1.txt', problem=1)
    stock_data.pop(0)

    start_date, end_date = mtb.input_dates()
    stock_data = mtb.filter_by_date(stock_data, start_date, end_date)
    stats = mtb.stock_data_to_date_stats(stock_data)
    mtb.write_to_csv("output_problem_1.csv", stats)
    mtb.plot_stats(stats)


def my_test():
    # problem_1()
    problem_2()


def main():
    my_test()


if __name__ == '__main__':
    main()
