# -*- coding: utf-8 -*-

# Description:
#
# This is the final version guess number game demo program
# The entry point of program starts with main() and being verified
# by __name__.
# The computer generated number is from 1 to x where x is passed
# from guess(x) and x is set to 10 in this listing.
#
# Author: Brian Lai
# Date: 18 Oct. 2021

import random   # import the random number package

def main():
    guess(10)
    
def guess(x):
    random_number = random.randint(1, x)
    guess = 0
    while guess != random_number:
        guess = int(input("Enter your guess number, please: "))
        check_win(guess,random_number)

def check_win(guess, rand_number):
    while guess == rand_number:
        return print("Correct!")
    else:
        return display_condition(guess, rand_number)

def display_condition(guess, rand_number):
    while guess < rand_number:
        return print("too low")
    else: 
        return print("too high")

if __name__ == '__main__':
    main()